const template = document.createElement( "template" );
template.innerHTML = `
     <style>
        :host{
            display: flex;
            justify-content: center;
            width: 100%;
        }

        :host( [empty] ) table#list-table {
            display: none
        }

        :host( [empty] ) label#empty {
            display: block;
        }

        table#list-table {            
            width: 100%;

            border: 1px solid black;
            border-spacing: 0;
            border-collapse: collapse;
        }

        tr[has-click-listener]:hover {
            cursor: pointer;
            background-color: #ececec;
        }

        th {
            background-color: darkgrey;
        }

        th[sort='1']::after {
            content: '\\02C5';
        }

        th[sort='-1']::after {
            content: '\\02C4';
        }

        th::after {
            display: inline-block;
            font-size: .85rem;
            margin-left: .5rem;
            font-weight: bold;
        }

        td, th {
            border: 1px solid black;
        }

        th:hover {
            background-color: grey;
            cursor: pointer;
        }

        label#empty {
            display: none;
        }

    </style>
    
    <slot id="header-slot"></slot>
    
    <table id="list-table" part='list-table'></table>
    <label id='empty' part='empty-label'>No Results to Show</label>
`;

window.ShadyCSS && window.ShadyCSS.prepareTemplate( template, "od-smart-list" );

export class OdSmartList extends HTMLElement {
    constructor() {
        super();
        //constructor creates the shadowRoot
        window.ShadyCSS && window.ShadyCSS.styleElement( this );
        if ( !this.shadowRoot ) {
            this.attachShadow( { mode: "open" } );
            this.shadowRoot.appendChild( template.content.cloneNode( true ) );
        }
    }

    connectedCallback() {        
        this._upgradeProperty( "row-id" );
        this._upgradeProperty( "route" );        
        this._upgradeProperty( "data" );

        let config = { attributes: false, childList: true, subtree: true };
        this._observer = new MutationObserver( ( mutationsList, observer ) => { 
            this._mutationObserverCallback( mutationsList, observer ) 
        } );
        this._observer.observe( this.shadowRoot.querySelector( '#header-slot' ), config );

        this._rows = [];
        this._sortKey = '';
        this._sortDir = 0;

        this._init();        
    }

    disconnectedCallback() {
        this._observer.disconnect();
    }

    async _init() {
        this._headers = await this._awaitHeaders();
        this._fieldNames = this._getFieldNames();
        this.shadowRoot.querySelector( '#list-table' ).appendChild( this._buildHeaderRow() );
        if( this.route ) {
            this._getList();
        } else {
            this._buildList();
        }
    }

    _upgradeProperty( prop ) {
        if ( Object.prototype.hasOwnProperty.call( this, prop ) ) {
            var value = this[prop];
            delete this[prop];
            this[prop] = value;
        }
    }

    static get observedAttributes() {
        return ["route"];
    }

    attributeChangedCallback( attrName, oldValue, newValue ) {
        switch ( attrName ) {
            case "route": {
                if( newValue !== oldValue ) {                    
                    this._setRouteAttribute( newValue );
                    this._getList();
                }
                break;
            }
        }
    }

    _mutationObserverCallback( mutationsList, observer ) {
        this.dispatchEvent(
            new CustomEvent( 'od-smart-list-mutation', { 
                bubbles: true, 
                detail: { 
                    'mutationsList': mutationsList,
                    'observer': observer
                }
            } )
        );
    }

    async _mutationPromise() {
        return new Promise( ( resolve ) => { 
            this.addEventListener( 'od-smart-list-mutation', ( e ) => { this._mutationPromiseCallback( e, resolve ); } );
        } );
    }

    _mutationPromiseCallback( e, resolve ) {
        e.stopPropagation();
        this.removeEventListener( 'od-smart-list-mutation', ( e ) => { this._mutationPromiseCallback( e, resolve ); } );
        resolve( e.detail ); 
    }

    async _awaitHeaders() {
        let headers = this._getHeaders();
        while( !headers || !headers.length || headers.length < 1 ) {
            await this._mutationPromise();
            headers = this._getHeaders();
        }
        return headers;
    }

    _getHeaders() {
        return this.shadowRoot.querySelector( '#header-slot' ).assignedNodes()
                                                                .filter( ( node ) => {
                                                                    return ( node.nodeType === Node.ELEMENT_NODE )
                                                                });
    }

    _getFieldNames() {
        return this._headers.map( ( h ) => {
            return h.getAttribute( 'name' );
        } );
    }

    _buildHeaderRow() {
        let headerRow = document.createElement( 'tr' );
        headerRow.classList.add( 'header-row' );
        headerRow.setAttribute( 'part', 'header-row' );
        for( let i = 0; i < this._headers.length; i++ ) {
            headerRow.appendChild( this._createHeader( this._headers[i] ) );
        }
        return headerRow;
    }

    _createHeader( odHeader ) {
        let name = odHeader.getAttribute( 'name' );
        let header = document.createElement( 'th' );
        header.classList.add( 'header-cell' );
        header.setAttribute( 'part', 'header-cell ' + name );
        header.setAttribute( 'name', name );
        header.appendChild( odHeader.header );
        header.addEventListener( 'click', ( e ) => this._orderByClickListener( e ) );
        return header;
    }

    async _getList() {
        if( this.route ) {
            this._httpGetAsync( this.route );
            this._data = await this._httpPromise();
        }
        this.refresh();
    }

    async _buildList() {
        if( !this._fieldNames || !this.data || !this.data.length || this.data.length < 1 ) {
            this.setAttribute( 'empty', true );
            return;
        }
        this.removeAttribute( 'empty' );
        this._orderList();
        let table = this.shadowRoot.querySelector( '#list-table' );
        for( let i = 0; i < this.data.length; i++ ) {
            let entry = this.data[i];
            let row  = this._buildRow( entry );
            this._rows.push( row )
            table.appendChild( row );
        } 
    }

    _buildRow( data ) {
        let dataRow = document.createElement( 'tr' );
        dataRow.classList.add( 'data-row' );
        dataRow.setAttribute( 'part', 'data-row' );
        if( this.rowId ) {
            dataRow.setAttribute( 'id', data[this.rowId] );
        }
        for( let i = 0; i < this._fieldNames.length; i++ ) { //parse fieldnames first to preserve order
            let fieldName = this._fieldNames[i];
            if( data && Object.keys( data ).includes( fieldName ) ) {
                dataRow.appendChild( this._createDataCell( fieldName, data[fieldName] ) )
            } 
        }
        if( this._rowOnClick ) {
            this._addRowListener( dataRow )
        }
        return dataRow;
    }

    _createDataCell( fieldName, value ) {
        let dataCell = document.createElement( 'td' );
        dataCell.classList.add( 'data-cell' );
        dataCell.setAttribute( 'part', 'data-cell ' + fieldName );
        dataCell.setAttribute( 'name', fieldName );
        dataCell.innerText = value;
        return dataCell;
    }

    _deleteDataRows() {
        let rows = this.shadowRoot.querySelectorAll( '#list-table > .data-row' );
        let table = this.shadowRoot.querySelector( '#list-table' );
        for( let i = 0; i < rows.length; i++ ) {
            let row = rows[i];
            table.removeChild( row );
        }
    }

    _httpGetAsync( theUrl ) {
        var xmlHttp = new XMLHttpRequest();
        xmlHttp.onreadystatechange = () => { 
        if ( xmlHttp.readyState == 4 && xmlHttp.status == 200 )
            this.dispatchEvent(
                new CustomEvent( 'od-smart-list-request-done', { 
                    bubbles: true, 
                    detail: {
                        'response': JSON.parse( xmlHttp.responseText )
                    }
                } )
            );
        }
        xmlHttp.open( "GET", theUrl, true ); // true for asynchronous 
        xmlHttp.send( null );
    }

    async _httpPromise() {
        return new Promise( ( resolve ) => { 
            this.addEventListener( 'od-smart-list-request-done', ( e ) => { this._httpPromiseCallback( e, resolve ); } );
        } );
    }

    _httpPromiseCallback( e, resolve ) {
        e.stopPropagation();
        this.removeEventListener( 'od-smart-list-request-done', ( e ) => { this._httpPromiseCallback( e, resolve ); } );
        resolve( e.detail.response ); 
    }

    _orderList() {
        if( this._sortKey === "" ) {
            return;
        }
        for( let i = 0; i < this._headers.length; i++ ) {
            let header = this._headers[i];
            if( header.getAttribute( 'name' ) === this._sortKey ) {
                if( this._sortDir === -1 ) {
                    this._data.sort( ( a, b ) => { return this._compare( b, a, this._sortKey ) } );
                } else {
                    this._data.sort( ( a, b ) => { return this._compare( a, b, this._sortKey ) } );
                }
                break;
            }
        }
    }

    _compare( a, b, key ) {
        if( a[key] && b[key] ) {
            if ( a[key] < b[key] ) {
                return -1;
            }
            if ( a[key] > b[key] ){
                return 1;
            }            
        } else if( a[key] ) {
            return 1;
        } else if( b[key] ) {
            return -1
        }        
        return 0;
    }    

    _orderBy( sortElem ) {
        if( this._sortElem ) {
            this._sortElem.removeAttribute( 'sort' )
        }
        this._sortElem = sortElem;
        this._sortElem.setAttribute( 'sort', this._sortDir )
        this.refresh();        
    }

    _orderByClickListener( e ) {
        let header = e.target;
        let name = header.getAttribute( 'name' );
        if( name === this._sortKey && this._sortDir === 1 ) {
            this._sortDir = -1
        } else {
            this._sortDir = 1;
            this._sortKey = name;
        }
        this._orderBy( header )    
    }

    _addRowListener( row ) {
        row.addEventListener( 'click', ( e ) => {
            this._rowOnClick( e, row )
        } );
        row.setAttribute( 'has-click-listener', true );
    }
      
    //properties - all 'custom attributes' should have a getter and setter to reflect the attribute
    get route() {
        return this.getAttribute( "route" );
    }

    set route( route ) {
        this._setClosedAttribute( route );
    }

    _setClosedAttribute( newV ) {
        if ( !newV || newV === "" ) {
            this.removeAttribute( "route" );
        } else {
            this.setAttribute( "route", newV );
        }
    }

    get rowId() {
        return this.getAttribute( "row-id" );
    }

    set rowId( rowId ) {
        this._setRowIdAttribute( rowId );
    }

    _setRowIdAttribute( newV ) {
        if ( !newV || newV === "" ) {
            this.removeAttribute( "row-id" );
        } else {
            this.setAttribute( "row-id", newV );
        }
    }

    get data() {
        return this._data;
    }

    set data( data ) {
        if ( typeof data !== "object" ) {
            return;
        }
        this._data = data;
        this.refresh();
    }

    //Public Mehtods
    refresh() {
        this._deleteDataRows();
        this._buildList();
    }

    addRowClickListener( callback ) {
        if( !callback ) {
            return;
        }
        this._rowOnClick = callback; //callback( e, row )
        if( this._rows.length > 0 ) {
            for( let i = 0; i < this._rows.length; i++ ) {
                this._addRowListener( this._rows[i] );
            }
        }        
    }

    orderBy( name, dir ) {
        let sortElem = this.shadowRoot.querySelector( 'th[name="' + name + '"]' )
        this._sortKey = name;
        this._sortDir = dir;
        this._orderBy( sortElem )
    }
}
