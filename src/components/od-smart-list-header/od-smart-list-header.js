const template = document.createElement( "template" );
template.innerHTML = `
    <style>
       
        :host {
            display: none;
        }

    </style>

    <slot id='header-slot'></slot>
`;

window.ShadyCSS && window.ShadyCSS.prepareTemplate( template, "od-smart-list-header" );

export class OdSmartListHeader extends HTMLElement {
    constructor() {
        super();
        //constructor creates the shadowRoot
        window.ShadyCSS && window.ShadyCSS.styleElement( this );
        if ( !this.shadowRoot ) {
            this.attachShadow( { mode: "open" } );
            this.shadowRoot.appendChild( template.content.cloneNode( true ) );
        }
    }

    connectedCallback() {
        this._header = this.shadowRoot.querySelector( '#header-slot' ).assignedNodes()[0];
    }

    disconnectedCallback() {}

    static get observedAttributes() {}

    attributeChangedCallback( attrName, oldValue, newValue ) {}

    get header() {
        return this._header;
    }
}
