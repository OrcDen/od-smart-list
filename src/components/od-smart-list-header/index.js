import { OdSmartList } from "../od-smart-list/od-smart-list";
import { OdSmartListHeader } from "./od-smart-list-header";
if ( !customElements.get( "od-smart-list-header" ) ) {
    window.customElements.define( "od-smart-list-header", OdSmartListHeader );
}
