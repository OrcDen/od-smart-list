# <od-smart-list>

> A component to quickly define lists with headers based on a given route 

`<od-smart-list>` is a simple component that allows the user to define list headers and a route. The list refreshes whenever the route or given data changes. Easy for tooling routes and integrating search and filtering.

## Installation
- Install with [npm](https://www.npmjs.com/)

```
npm i @orcden/od-smart-list
```
## Usage
```
import '@orcden/od-smart-list';
```
```
<od-smart-list id='smart-list' row-id='id' route='https://example.com/route'>
    <od-smart-list-header name='field1'>Test Head 1</od-smart-list-header>
    <od-smart-list-header name='field2'>Test Head 2</od-smart-list-header>
</od-smart-list>
```
    
## Attributes
### OD-Smart-List
| Attribute | Type | Default | Description                                                                             |
|-----------|---------|---------|-----------------------------------------------------------------------------------------|
| `route`  | String  | undefined      | Set this to have the list automatically get data from a route  |
| `row-id`  | String  | undefined      | Will set each row's id to the field value indicated here. |

### OD-Smart-List-Header
| Attribute | Type | Default | Description                                                                             |
|-----------|---------|---------|-----------------------------------------------------------------------------------------|
| `name`  | String | undefined   | set this value to tell which field of data to pull for each column |

## Properties
### OD-Smart-List
| Attribute | Type | Default | Description                                                                             |
|-----------|---------|---------|-----------------------------------------------------------------------------------------|
| `route`  | String  | undefined      | Set this to have the list automatically get data from a route  |
| `rowId`  | String  | undefined      | will set each row's id to the field value indicated here. |
| `data`  | Array  | undefined   | Can be used to get and set the data displayed in the list |

## Functions
### OD-Smart-List
| Name | Parameters | Description                                  |
|-----------|------|-----------------------------------------------|
| `refresh`   | None | Manually refreshes the list. should not need to be called under most circumsatnces |
| `addRowClickListener`   | callback | Adds a click listener to each row. Callback parameters: event, row |
| `orderBy`   | string fieldName, int direction | orders the list based on the given header fieldname and direction ( 1 for ascending, -1 for descending ) |

## Styling
- CSS variables are available to alter the default styling provided

### OD-Smart-List
| Shadow Parts     | Description           |
|------------------|-----------------------|
| list-table           | The main table used for the list |
| header-row          | The header row |
| header-cell          | The cells in the header |
| data-row          | The rows that hold data |
| data-cell          | The cells that hold data |
| empty-label          | The label that displays the empty message |
| `fieldName`         | maps the 'name' of each header to a column of cells |

## Development
### Run development server and show demo

```
npm run demo
```

### Run linter

```
npm run lint
```

### Fix linter errors

```
npm run fix
```

### Run tests

```
npm run test
```

### Build for production

```
npm run build
```